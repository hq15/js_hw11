/*
Почему для работы с input не рекомендуется использовать события клавиатуры? - Потому что отслеживать эти события не надежно, так как ввод даных не обязательно будет использоваться при помощи клавиатуры. К примеру при помощи мышки, правого клика и меню и при этом не будет нажатий клавиш.
*/


const btnLettersArr = document.querySelectorAll('.btn');
document.addEventListener('keydown', function (event) {
    lookingForAKey(btnLettersArr, event.key, 'btn-active');
});

function lookingForAKey(arr, key, className) {
    arr.forEach((element) => {
        element.dataset.buttonKey === key
          ? element.classList.add(className)
          : element.classList.remove(className);
      });
}

// перебор массива и присвоение цвета из js
/*
document.addEventListener('keydown', function(event) {
    arrBtn = document.querySelectorAll('.btn');
    iterateArrayLetters(arrBtn, event.key);
      });

function iterateArrayLetters(arr, keyEvent) {
    arr.forEach(function(item, i, arr) {
        let content = arr[i].innerText;
        arr[i].style.backgroundColor = 'black'
        let letter = content.toLowerCase();
        let key = keyEvent.toLowerCase();
        (key === letter) ? arr[i].style.backgroundColor = 'red' : stop = false;
        });
};
*/